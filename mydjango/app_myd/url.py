from django.urls import path
from .views import index, ejemplo, saludar
urlpatterns = [
     path('', index),
     path('ej', ejemplo),
     path("<str:nombre>", saludar)
     
]