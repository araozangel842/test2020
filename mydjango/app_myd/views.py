from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
def index(request):
     # return HttpResponse("Hola Angel desde PYTHON")
     return render(request, "first/index.html")

# Todos estos enlaces son a partir del index
def ejemplo(request):
     return HttpResponse("Hola del ejemplo")

def saludar(request, nombre):
     return HttpResponse(f"Hola, {nombre.capitalize()}")