from django.shortcuts import render
import datetime

# Create your views here.
def index(request):
     date_now = datetime.datetime.now()
     return render(request, "newyear/index.html", {
          # "new": date_now.month == 1 and date_now.day == 1
          "new": True
     })